<?php

if (file_exists(__DIR__ . "/LocalSettings.php")) {
    require_once __DIR__ . "/LocalSettings.php";
} elseif (file_exists("/var/www/pastell/test/PHPUnit/init.php")) {
    require_once "/var/www/pastell/test/PHPUnit/init.php";
} else {
    //phpcs:ignore Generic.Files.LineLength.TooLong
    throw new Exception("Impossible d'initialiser Pastell ni avant l'emplacement du fichier PHPUnit/init.php par défaut ni de charger dans un fichier LocalSettings.php");
}

require_once __DIR__ . "/PackRHTestCase.class.php";
