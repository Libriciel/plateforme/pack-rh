<?php

class NextCloudTest extends PackRHTestCase
{
    use CurlUtilitiesTestTrait;

    /**
     * @throws Exception
     */
    private function createNextCloudConnector(): Nextcloud
    {
        $id_ce = $this->createConnector('depot-nextcloud', 'Dépôt Nextcloud')['id_ce'];

        $this->configureConnector($id_ce, [
            'depot_api_rest_url' => 'https://foo/',
            'depot_api_rest_token' => 'bar',
            'depot_webdav_login' => 'baz',
            'annuaire_url' => 'https://fusion/',
            'annuaire_password' => 'barbar',
            'annuaire_filter' => '(sn={{ nom_usage_agent }})',
            'annuaire_output_attribute' => 'roomNumber',
            'annuaire_test' => 'nom_usage_agent: Foo
prenom_usage_agent: Bar
annee_bulletin: 2021
mois_bulletin: septembre
siret_collectivite: 256 910 183 00027
matricule_agent: 007',
            'depot_directory' =>
                '/{{ siret_collectivite }}-{{ matricule_agent }}/{{ annee_bulletin }}/Bulletin de salaire/'
        ]);

        /** @var Nextcloud $nextCloud */
        $nextCloud = $this->getConnecteurFactory()->getConnecteurById($id_ce);
        return $nextCloud;
    }


    /**
     * @throws UnrecoverableException
     * @throws RecoverableException
     * @throws Exception
     */
    public function testConnectOK(): void
    {
        $this->mockCurl([
            'https://foo//ocs/v1.php/cloud/users/baz' =>
                file_get_contents(__DIR__ . "/fixtures/get_user_responses.xml"),
        ]);

        $nextCloud = $this->createNextCloudConnector();
        $result = $nextCloud->searchUser();
        self::assertStringEqualsFile(__DIR__ . '/fixtures/get_user_responses.xml', $result);
    }

    /**
     * @throws UnrecoverableException
     * @throws Exception
     */
    public function testConnectUnauthorized(): void
    {
        $this->mockCurl([
            'https://foo//ocs/v1.php/cloud/users/baz' =>
                file_get_contents(__DIR__ . "/fixtures/get_user_responses_unauthorized.xml"),
        ], 401);
        $nextCloud = $this->createNextCloudConnector();
        $this->expectException(RecoverableException::class);
        $nextCloud->searchUser();
    }

    public function testAnnuaire(): void
    {
        $this->mockCurl([
            'https://fusion//v1/login' =>
               "xyzt",
            'https://fusion//v1/objects/user?filter=%28sn%3DFoo%29&attrs=roomNumber' =>
                '{"uid=foo@bar.local,ou=users,ou=ad,ou=foo,dc=test,dc=fr":"D3DBFD5A-B995-4900-B688-DFB91F699832"}'
        ]);
        $nextCloud = $this->createNextCloudConnector();
        self::assertEquals("D3DBFD5A-B995-4900-B688-DFB91F699832", $nextCloud->testAnnuaire());
    }

    public function testSaveDocument(): void
    {
        $webdavWrapper = $this->createMock('WebdavWrapper');
        $this->getObjectInstancier()->setInstance(WebdavWrapper::class, $webdavWrapper);
        $this->mockCurl([
            'https://fusion//v1/login' =>
                "xyzt",
            'https://fusion//v1/objects/user?filter=%28sn%3DFoo%29&attrs=roomNumber' =>
                '{"uid=foo@bar.local,ou=users,ou=ad,ou=foo,dc=test,dc=fr":"D3DBFD5A-B995-4900-B688-DFB91F699832"}',
            'https://foo//ocs/v2.php/apps/files_sharing/api/v1/shares' =>
                file_get_contents(__DIR__ . "/fixtures/share_folder_response.xml")
        ]);
        $tmpFolder = new TmpFolder();
        $tmp_folder = $tmpFolder->create();
        file_put_contents("$tmp_folder/test.txt", "foo");

        $nextCloud = $this->createNextCloudConnector();
        self::assertEquals(
            "/256%20910%20183%2000027-007/2021/Bulletin%20de%20salaire//test.txt",
            $nextCloud->saveDocument("bar", "/test.txt", "$tmp_folder/test.txt")
        );
    }
}
