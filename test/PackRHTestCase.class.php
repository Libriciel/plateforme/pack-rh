<?php

class PackRHTestCase extends PastellTestCase
{
    public function reinitDatabase()
    {
        parent::reinitDatabase();
        $this->getObjectInstancier()->getInstance(ExtensionLoader::class)->loadExtension([__DIR__ . "/../"]);
    }
}
