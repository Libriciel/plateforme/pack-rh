<?php

use Pastell\Service\SimpleTwigRenderer;

require_once __DIR__ . "/lib/FusionDirectory.php";

class Nextcloud extends DepotWebDAV
{
    private $curlWrapperFactory;
    private $simpleTwigRenderer;
    private $donneesFormulaireFactory;
    private $fusionDirectory;
    private $depot_api_rest_url;
    private $depot_api_rest_token;
    private $depot_webdav_login;

    private $depot_user_share_directory;

    private $annuaire_url;
    private $annuaire_password;
    private $annuaire_filter;
    private $annuaire_output_attribute;
    private $annuaire_test;
    private $depot_directory;

    public function __construct(
        WebdavWrapper $webdavWrapper,
        CurlWrapperFactory $curlWrapperFactory,
        SimpleTwigRenderer $simpleTwigRenderer,
        DonneesFormulaireFactory $donneesFormulaireFactory,
        FusionDirectory $fusionDirectory
    ) {
        parent::__construct($webdavWrapper);
        $this->curlWrapperFactory = $curlWrapperFactory;
        $this->simpleTwigRenderer = $simpleTwigRenderer;
        $this->donneesFormulaireFactory = $donneesFormulaireFactory;
        $this->fusionDirectory = $fusionDirectory;
    }

    public function setConnecteurConfig(DonneesFormulaire $connecteurConfig)
    {
        parent::setConnecteurConfig($connecteurConfig);
        $this->depot_api_rest_url = $connecteurConfig->get('depot_api_rest_url');
        $this->depot_api_rest_token = $connecteurConfig->get('depot_api_rest_token');
        $this->depot_webdav_login = $connecteurConfig->get('depot_webdav_login');
        $this->annuaire_url = $connecteurConfig->get('annuaire_url');
        $this->annuaire_password = $connecteurConfig->get('annuaire_password');
        $this->annuaire_filter = $connecteurConfig->get('annuaire_filter');
        $this->annuaire_output_attribute = $connecteurConfig->get('annuaire_output_attribute');
        $this->annuaire_test = $connecteurConfig->get('annuaire_test');
        $this->depot_user_share_directory = trim($connecteurConfig->get('depot_user_share_directory'), "/");
        $this->depot_directory = trim($connecteurConfig->get('depot_directory'), "/");
    }

    /**
     * @throws UnrecoverableException
     * @throws RecoverableException
     */
    public function searchUser(): string
    {
        $curl = $this->curlWrapperFactory->getInstance();
        $curl->addHeader('OCS-APIRequest', 'true');
        $curl->httpAuthentication($this->depot_webdav_login, $this->depot_api_rest_token);
        $url = sprintf("%s/ocs/v1.php/cloud/users/%s", $this->depot_api_rest_url, $this->depot_webdav_login);
        $result = $curl->get($url);
        if ($curl->getHTTPCode() !== 200) {
            throw new RecoverableException(sprintf(
                "Code HTTP retourné : %d\n--\nRéponse : %s",
                $curl->getHTTPCode(),
                htmlentities($result)
            ));
        }
        $xmlWrapper = new SimpleXMLWrapper();
        try {
            $xml = $xmlWrapper->loadString($result);
        } catch (SimpleXMLWrapperException $XMLWrapperException) {
            throw new UnrecoverableException(
                sprintf("Impossible d'analyser le contenu XML de la réponse : %s", $XMLWrapperException->getMessage())
            );
        }
        if (isset($xml->meta->status) && $xml->meta->status != "ok") {
            throw new UnrecoverableException(sprintf("Réponse inattendue : %s", htmlentities($result)));
        }
        return $result;
    }

    public function shareFolder(string $folder_path, string $user)
    {
        $curl = $this->curlWrapperFactory->getInstance();
        $curl->addHeader('OCS-APIRequest', 'true');
        $curl->httpAuthentication($this->depot_webdav_login, $this->depot_api_rest_token);
        $curl->addPostData('path', $folder_path);
        $curl->addPostData('shareType', 0);
        $curl->addPostData('shareWith', $user);
        $url = sprintf("%s/ocs/v2.php/apps/files_sharing/api/v1/shares", $this->depot_api_rest_url);
        $result = $curl->get($url);
        if ($curl->getHTTPCode() !== 200) {
            throw new RecoverableException(sprintf(
                "Code HTTP retourné : %d\n--\nRéponse : %s",
                $curl->getHTTPCode(),
                htmlentities($result)
            ));
        }
        $xmlWrapper = new SimpleXMLWrapper();
        try {
            $xml = $xmlWrapper->loadString($result);
        } catch (SimpleXMLWrapperException $XMLWrapperException) {
            throw new UnrecoverableException(
                sprintf("Impossible d'analyser le contenu XML de la réponse : %s", $XMLWrapperException->getMessage())
            );
        }
        if (isset($xml->meta->status) && $xml->meta->status != "ok") {
            throw new UnrecoverableException(sprintf("Réponse inattendue : %s", htmlentities($result)));
        }

        return $result;
    }
    private $forTesting = true;
    public function setDocDonneesFormulaire(DonneesFormulaire $docDonneesFormulaire): void
    {
        parent::setDocDonneesFormulaire($docDonneesFormulaire);
        $this->forTesting = false;
    }

    public function getUser()
    {
        if (! $this->forTesting) {
            $donneesFormulaire = $this->getDocDonneesFormulaire();
            $dictionnary = [];
        } else {
            $donneesFormulaire = $this->donneesFormulaireFactory->getNonPersistingDonneesFormulaire();
            $dictionnary = $this->getTestMetadata();
        }

        $expression = $this->simpleTwigRenderer->render($this->annuaire_filter, $donneesFormulaire, $dictionnary);
        return $this->fusionDirectory->search(
            $this->annuaire_url,
            $this->annuaire_password,
            $expression,
            $this->annuaire_output_attribute
        );
    }

    public function testAnnuaire(): string
    {
        $donneesFormulaire = $this->donneesFormulaireFactory->getNonPersistingDonneesFormulaire();
        return $this->getUser($donneesFormulaire, $this->getTestMetadata());
    }

    public function getTestMetadata()
    {
        $dictionnary = $this->annuaire_test;
        $info = [];
        foreach (explode("\n", $dictionnary) as $line) {
            $word = explode(":", $line, 2);
            if (empty($word[0])) {
                continue;
            }
            $id = trim($word[0]);
            $value = trim($word[1] ?? '');
            $info[$id] = $value;
        }
        return $info;
    }

    public function saveDocument(string $directory_name, string $filename, string $filepath)
    {
        $directory = $this->getDirectory($this->depot_directory);
        $directory_cumul = "";
        foreach (explode("/", $directory) as $directory_part) {
            $directory_cumul .= trim($directory_part, "/");
            if (! $this->directoryExists($directory_cumul)) {
                $this->makeDirectory($directory_cumul);
            }
            $directory_cumul .= "/";
        }

        $result = parent::saveDocument($directory, $filename, $filepath);
        $directory = $this->getDirectory($this->depot_user_share_directory);
        $this->shareFolder(rawurldecode($directory), $this->getUser());
        return $result;
    }

    public function fileExists(string $filename)
    {
        $directory = $this->getDirectory($this->depot_directory);
        return $this->directoryExists($directory . "/" . $filename);
    }

    private function getDirectory(string $directory)
    {
        if (! $this->forTesting) {
            $donneesFormulaire = $this->getDocDonneesFormulaire();
            $dictionnary = [];
        } else {
            $donneesFormulaire = $this->donneesFormulaireFactory->getNonPersistingDonneesFormulaire();
            $dictionnary = $this->getTestMetadata();
        }

        $share_directory = $this->simpleTwigRenderer->render($directory, $donneesFormulaire, $dictionnary);

        $result = "";
        foreach (explode("/", trim($share_directory, "/")) as $part) {
            $result .= rawurlencode($part) . "/";
        }
        return trim($result, "/");
    }
}
