<?php

class NextCloudTestFusionDirectory extends ActionExecutor
{
    public function go()
    {
        /** @var Nextcloud $nextCloud */
        $nextCloud = $this->getMyConnecteur();
        $result = $nextCloud->testAnnuaire();
        $this->setLastMessage(
            "Attribut récupéré pour l'utilisateur défini par les données de test : <b>$result</b>"
        );
        return true;
    }
}
